package AbilitiesApi
import Assets
import AbilityObjEditing
import HashMap

public constant ID_SELECT                             = 'A001'
public constant ID_GENERIC_JUMP                       = 'A00B'
public constant ID_CREATE_ILLUSION                    = 'A00G'
public constant ID_STUN                               = 'A00N'
public constant ID_ORCISH_SWORDMASTER_ATTACK          = 'A00D'
public constant ID_ORCISH_SWORDMASTER_ESCAPE          = 'A000'
public constant ID_ORCISH_SWORDMASTER_HALL_OF_MIRRORS = 'A00E'
public constant ID_ORCISH_SWORDMASTER_WAR_SLAM        = 'A00R'
public constant ID_ORCISH_SWORDMASTER_IMPALE          = 'A00F'
public constant ID_ORCISH_SWORDMASTER_HAND_FX         = 'A00H'
public constant ID_DEATHS_MAGE_ATTACK                 = 'A002'
public constant ID_DEATHS_MAGE_REDLINE                = 'A007'
public constant ID_DEATHS_MAGE_REAP                   = 'A008'
public constant ID_DEATHS_MAGE_REAP_INVISIBILITY      = 'A004'
public constant ID_DEATHS_MAGE_WEAPON_EFFECT          = 'A00C'
public constant ID_DEATHS_MAGE_PURSUIT                = 'A003'
public constant ID_DEATHS_MAGE_INVOKE_SOULS           = 'A005'
public constant ID_SHADOW_SEER_ATTACK                 = 'A00O'
public constant ID_SHADOW_SEER_STORM                  = 'A00I'
public constant ID_SHADOW_SEER_FOOT_FX                = 'A00M'
public constant ID_SHADOW_SEER_SHADOW_WARD            = 'A006'
public constant ID_SHADOW_SEER_SHADOW_WARD_GREEN      = 'A009'
public constant ID_SHADOW_SEER_FAR_ATTACK             = 'A00A'
public constant ID_UNDERLORD_BLADE                    = 'Abld'
public constant ID_UNDERLORD_SWIRL                    = 'Aswl'
public constant ID_UNDERLORD_BLOCK                    = 'Ablk'
public constant ID_UNDERLORD_FROZEN_STATUE            = 'Afrs'
public constant ID_HIGH_NAGA_INVIGILATOR_ATTACK       = 'A00J'
public constant ID_HIGH_NAGA_INVIGILATOR_TORRENT      = 'A00K'
public constant ID_HIGH_NAGA_INVIGILATOR_COUNTER      = 'A00L'
public constant ID_HIGH_NAGA_INVIGILATOR_FAKE_COUNTER = 'A00P'
public constant ID_HIGH_NAGA_INVIGILATOR_BLADE_ARTIST = 'A00Q'

tuple int_string(int id, string descr)

public function asMap(vararg int_string pairs) returns IterableMap<int, string>
    let map = new IterableMap<int, string>()
    for pair in pairs
        map.put(pair.id, pair.descr)
    return map

public constant ABILITIES_ORCISH_SWORDMASTER = asMap(
    int_string(ID_ORCISH_SWORDMASTER_ESCAPE,          "Escape"),
    int_string(ID_ORCISH_SWORDMASTER_HALL_OF_MIRRORS, "Hall of Mirrors"),
    int_string(ID_ORCISH_SWORDMASTER_WAR_SLAM,        "War Slam"),
    int_string(ID_ORCISH_SWORDMASTER_IMPALE,          "Impale")
)

public constant ABILITIES_DEATHS_MAGE = asMap(
    int_string(ID_DEATHS_MAGE_PURSUIT,      "Pursuit"),
    int_string(ID_DEATHS_MAGE_REAP,         "Reap"),
    int_string(ID_DEATHS_MAGE_INVOKE_SOULS, "Invoke Souls"),
    int_string(ID_DEATHS_MAGE_REDLINE,      "Barrage")
)

public constant ABILITIES_SHADOW_SEER = asMap(
    int_string(ID_SHADOW_SEER_SHADOW_WARD,       "Lightning Ward"),
    int_string(ID_SHADOW_SEER_SHADOW_WARD_GREEN, "Shadow Ward"),
    int_string(ID_SHADOW_SEER_FAR_ATTACK,        "Far Attack"),
    int_string(ID_SHADOW_SEER_STORM,             "Dark Storm")
)

public constant ABILITIES_UNDERLORD = asMap(
    int_string(ID_UNDERLORD_BLADE,         "Blade"),
    int_string(ID_UNDERLORD_SWIRL,         "Swirl"),
    int_string(ID_UNDERLORD_BLOCK,         "Block"),
    int_string(ID_UNDERLORD_FROZEN_STATUE, "Frozen Statue")
)

public constant ABILITIES_HIGH_NAGA_INVIGILATOR = asMap(
    int_string(ID_HIGH_NAGA_INVIGILATOR_TORRENT,      "Torrent"),
    int_string(ID_HIGH_NAGA_INVIGILATOR_COUNTER,      "Counter"),
    int_string(ID_HIGH_NAGA_INVIGILATOR_FAKE_COUNTER, "Fake Counter"),
    int_string(ID_HIGH_NAGA_INVIGILATOR_BLADE_ARTIST, "Blade Artist")
)

@compiletime function create_w3a_A001()
    let _ = createObjectDefinition("w3a", ID_SELECT, 'Aivs')
    ..setLvlDataInt("abpx", 0, 0, 0)
    ..setLvlDataString("aart", 0, 0, "ReplaceableTextures\\CommandButtons\\BTNGlove.blp")
    ..setLvlDataString("atat", 0, 0, "")
    ..setLvlDataUnreal("aran", 1, 0, 250.)
    ..setLvlDataUnreal("ahdu", 1, 0, 1.)
    ..setLvlDataUnreal("adur", 1, 0, 1.)
    ..setLvlDataInt("amcs", 1, 0, 0)
    ..setLvlDataString("areq", 0, 0, "")
    ..setLvlDataString("anam", 0, 0, "Select")
    ..setLvlDataString("atp1", 1, 0, "Select")
    ..setLvlDataString("aub1", 1, 0, "Select a hero.")
    ..setLvlDataString("ahky", 0, 0, "Q")
    ..setLvlDataString("atar", 1, 0, "ground,neutral,vulnerable,structure")


@compiletime function create_w3a_A000()
    let name = ABILITIES_ORCISH_SWORDMASTER.get(ID_ORCISH_SWORDMASTER_ESCAPE)
    let _ = createObjectDefinition("w3a", ID_ORCISH_SWORDMASTER_ESCAPE, 'AEfk')
    ..setLvlDataInt("aher", 0, 0, 0)
    ..setLvlDataInt("alev", 0, 0, 1)
    ..setLvlDataString("anam", 0, 0, name)
    ..setLvlDataString("ahky", 0, 0, "Q")
    ..setLvlDataString("atp1", 1, 0, name)
    ..setLvlDataString("aub1", 1, 0, "Activate to backflip away from battle in a cloud of smoke.|n |n|cff999999Cooldown: |r10s")
    ..setLvlDataUnreal("Efk1", 1, 1, 0.)
    ..setLvlDataUnreal("Efk4", 1, 4, 0.)
    ..setLvlDataUnreal("Efk2", 1, 2, 0.)
    ..setLvlDataUnreal("aare", 1, 0, 0.01)
    ..setLvlDataUnreal("acdn", 1, 0, 10.)
    ..setLvlDataInt("amcs", 1, 0, 0)
    ..setLvlDataString("arac", 0, 0, "human")
    ..setLvlDataString("atar", 1, 0, "none")
    ..setLvlDataString("aeat", 0, 0, "")
    ..setLvlDataString("aart", 0, 0, "ReplaceableTextures\\CommandButtons\\BTNDisenchant.blp")
    ..setLvlDataUnreal("amac", 0, 0, 0.)
    ..setLvlDataString("amat", 0, 0, "")
    ..setLvlDataInt("amsp", 0, 0, 0)
    ..setLvlDataString("aani", 0, 0, "")

@compiletime function create_w3a_A00E()
    let name = ABILITIES_ORCISH_SWORDMASTER.get(ID_ORCISH_SWORDMASTER_HALL_OF_MIRRORS)
    let _ = createObjectDefinition("w3a", ID_ORCISH_SWORDMASTER_HALL_OF_MIRRORS, 'ANbf')
    ..setLvlDataString("aani", 0, 0, "")
    ..setLvlDataInt("abpx", 0, 0, 1)
    ..setLvlDataInt("alev", 0, 0, 1)
    ..setLvlDataInt("aher", 0, 0, 0)
    ..setLvlDataString("aart", 0, 0, "ReplaceableTextures\\CommandButtons\\BTNMirrorImage.blp")
    ..setLvlDataString("amat", 0, 0, "")
    ..setLvlDataInt("amsp", 0, 0, 1000)
    ..setLvlDataUnreal("Ucs1", 1, 1, 0.01)
    ..setLvlDataUnreal("Nbf5", 1, 5, 0.)
    ..setLvlDataUnreal("Ucs3", 1, 3, 1000.)
    ..setLvlDataUnreal("Ucs4", 1, 4, 100.)
    ..setLvlDataUnreal("Ucs2", 1, 2, 1.)
    ..setLvlDataUnreal("aare", 1, 0, 100.)
    ..setLvlDataString("abuf", 1, 0, "")
    ..setLvlDataUnreal("aran", 1, 0, 1000.)
    ..setLvlDataUnreal("ahdu", 1, 0, 0.01)
    ..setLvlDataUnreal("adur", 1, 0, 0.01)
    ..setLvlDataInt("amcs", 1, 0, 0)
    ..setLvlDataString("arac", 0, 0, "human")
    ..setLvlDataString("atar", 1, 0, "ground")
    ..setLvlDataString("ahky", 0, 0, "W")
    ..setLvlDataString("anam", 0, 0, name)
    ..setLvlDataString("atp1", 1, 0, name)
    ..setLvlDataString(
        "aub1",
        1,
        0,
        "Dashes in the target direction at a blurring speed. If the user collides with a unit, it will be thrown. At the end of the dash, the swordsman will spawn an illusionary double, which lasts a short time and deals no damage."
        + "|n |n|cff999999Range: |r1,000"
        + "|n|cff999999Cooldown: |r12s"
        + "|n|cff999999Throw: |rMedium"
    )
    ..setLvlDataUnreal("acdn", 1, 0, 12.)

@compiletime function create_w3a_A00F()
    let name = ABILITIES_ORCISH_SWORDMASTER.get(ID_ORCISH_SWORDMASTER_IMPALE)
    let _ = createObjectDefinition("w3a", ID_ORCISH_SWORDMASTER_IMPALE, 'AHtb')
    ..setLvlDataString("aani", 0, 0, "")
    ..setLvlDataInt("abpx", 0, 0, 3)
    ..setLvlDataInt("alev", 0, 0, 1)
    ..setLvlDataInt("aher", 0, 0, 0)
    ..setLvlDataString("aart", 0, 0, "ReplaceableTextures\\CommandButtons\\BTNArcaniteMelee.blp")
    ..setLvlDataString("amat", 0, 0, "")
    ..setLvlDataUnreal("Htb1", 1, 1, 0.)
    ..setLvlDataString("abuf", 1, 0, "")
    ..setLvlDataUnreal("aran", 1, 0, 115.)
    ..setLvlDataUnreal("acdn", 1, 0, 16.)
    ..setLvlDataUnreal("ahdu", 1, 0, 0.01)
    ..setLvlDataUnreal("adur", 1, 0, 0.01)
    ..setLvlDataInt("amcs", 1, 0, 0)
    ..setLvlDataString("atar", 1, 0, "enemies,ground,vulnerable")
    ..setLvlDataString("anam", 0, 0, name)
    ..setLvlDataString("ansf", 0, 0, "(Orcish Swordmaster)")
    ..setLvlDataString("ahky", 0, 0, "R")
    ..setLvlDataString("atp1", 1, 0, name)
    ..setLvlDataString(
        "aub1",
        1,
        0,
        "Launches the target enemy into the air. Shortly afterwards, the users deals a powerful blow."
        + "|n |n|cff999999Range: |rMelee"
        + "|n|cff999999Cooldown: |r16s"
        + "|n|cff999999Power: |r200"
        + "|n|cff999999Throw: |rExtreme"
    )


@compiletime function create_w3a_A00H()
    let _ = createObjectDefinition("w3a", ID_ORCISH_SWORDMASTER_HAND_FX, 'AId1')
    ..setLvlDataString("ansf", 0, 0, "(Orcish Swordsman)")
    ..setLvlDataString("anam", 0, 0, "Hand Effect")
    ..setLvlDataInt("Idef", 1, 1, 0)
    ..setLvlDataInt("aite", 0, 0, 0)
    ..setLvlDataString("atat", 0, 0, "war3mapImported\\VikingSword_byKitabatake.mdx")
    ..setLvlDataString("ata0", 0, 0, "hand,left")
    ..setLvlDataString("arac", 0, 0, "human")


@compiletime function create_w3a_A00C()
    let _ = createObjectDefinition("w3a", ID_DEATHS_MAGE_WEAPON_EFFECT, 'AId2')
    ..setLvlDataString("ansf", 0, 0, "(Death's Mage)")
    ..setLvlDataString("anam", 0, 0, "Weapon Effect")
    ..setLvlDataInt("Idef", 1, 1, 0)
    ..setLvlDataString("atat", 0, 0, "Abilities\\Weapons\\SerpentWardMissile\\SerpentWardMissile.mdl")
    ..setLvlDataString("ata0", 0, 0, "weapon")
    ..setLvlDataInt("aite", 0, 0, 0)
    ..setLvlDataString("arac", 0, 0, "human")
    ..setLvlDataString("aart", 0, 0, "")


@compiletime function create_w3a_A00M()
    let _ = createObjectDefinition("w3a", ID_SHADOW_SEER_FOOT_FX, 'AItg')
    ..setLvlDataString("ansf", 0, 0, "(Shadow Seer)")
    ..setLvlDataString("anam", 0, 0, "Foot Effect")
    ..setLvlDataInt("Iatt", 1, 1, 0)
    ..setLvlDataInt("aite", 0, 0, 0)
    ..setLvlDataString("aart", 0, 0, "")
    ..setLvlDataString("atat", 0, 0, "Abilities\\Weapons\\AvengerMissile\\AvengerMissile.mdl,Abilities\\Weapons\\AvengerMissile\\AvengerMissile.mdl")
    ..setLvlDataString("ata0", 0, 0, "foot,left")
    ..setLvlDataString("ata1", 0, 0, "foot,right")
    ..setLvlDataString("arac", 0, 0, "human")

@compiletime function create_w3a_A00G()
    let _ = createObjectDefinition("w3a", ID_CREATE_ILLUSION, 'AIil')
    ..setLvlDataString("aart", 0, 0, "")
    ..setLvlDataString("atat", 0, 0, "")
    ..setLvlDataString("ata0", 0, 0, "")
    ..setLvlDataUnreal("Iilw", 1, 2, 1.)
    ..setLvlDataInt("aite", 0, 0, 0)
    ..setLvlDataString("arac", 0, 0, "human")
    ..setLvlDataString("anam", 0, 0, "ILLUSIONCREATE")
    ..setLvlDataUnreal("aran", 1, 0, 99999.)

@compiletime function create_w3a_A00N()
    let _ = createObjectDefinition("w3a", ID_STUN, 'AHtb')
    ..setLvlDataInt("abpy", 0, 0, 0)
    ..setLvlDataString("aani", 0, 0, "")
    ..setLvlDataString("aart", 0, 0, "")
    ..setLvlDataString("arar", 0, 0, "")
    ..setLvlDataString("amat", 0, 0, "")
    ..setLvlDataInt("amsp", 0, 0, 99999)
    ..setLvlDataInt("aher", 0, 0, 0)
    ..setLvlDataInt("alev", 0, 0, 1)
    ..setLvlDataUnreal("Htb1", 1, 1, 0.)
    ..setLvlDataUnreal("aran", 1, 0, 99999.)
    ..setLvlDataUnreal("acdn", 1, 0, 1.)
    ..setLvlDataUnreal("ahdu", 1, 0, 2.)
    ..setLvlDataUnreal("adur", 1, 0, 2.)
    ..setLvlDataInt("amcs", 1, 0, 0)
    ..setLvlDataString("atar", 1, 0, "air,enemies,friend,ground,neutral,vulnerable")
    ..setLvlDataString("ahky", 0, 0, "")
    ..setLvlDataString("anam", 0, 0, "COLLISONSTUN")
    ..setLvlDataString("atp1", 1, 0, "")
    ..setLvlDataString("aub1", 1, 0, "")

@compiletime function create_w3h_jump_buff()
    let _ = createObjectDefinition("w3h", 'BZZZ', 'BHds')
    ..setString("ftat", "")

class GenericJump extends AbilityDefinitionPaladinDivineShield
    construct()
        super(ID_GENERIC_JUMP)
        presetCanDeactivate(_ -> false)
        setName("Jump")
        setHeroAbility(false)
        presetButtonPosNormal(1, 1)
        presetButtonPosTurnOff(1, 1)
        setIconNormal(Icons.bTNBoots)
        setIconTurnOff(Icons.bTNBoots)
        presetBuffs(_ -> "BZZZ")
        setArtCaster("")
        setArtTarget("")
        setAnimationNames("")
        presetTooltipNormal(_ -> "Jump")
        presetTooltipNormalExtended(
            _ -> "Generic jump ability. The user's next move order jumps in the target direction, rather than walking."
            + "|n |n|cff999999Cooldown: |r10s"
        )
        presetHotkey("F")
        setEffectSound("")
        presetDurationHero(_ -> 0.1)
        presetCooldown(_ -> 10.)
        presetManaCost(_ -> 0)

@compiletime function inject()
    new GenericJump()
