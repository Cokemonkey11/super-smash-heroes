library Knockback
    import TerrainUtils

    import AbilitiesApi
    import UnitsApi

    struct KnockDat
        unit u
        real xOffs
        real yOffs
        real zOffs
    endstruct

    globals
        constant integer CROWID='Arav'
        constant real BOUNCECOEFFICIENT=.4
        constant real FRICTION=.15
        constant real GRAVITY=1.375
        constant real MAXZVELOCITYTOBOUNCE=-10.
        constant real MINFLYHEIGHT=5.
        constant real MINFORKNOCKBACK=1.
        constant real MINFORSTUN=7.
        constant real MINFRICTIONFOREFFECTSQUARED=9.
        constant real MINZVELOCITYTOBECOMEAIRBORNE=5.
        constant string FRICTIONMODEL="Objects\\Spawnmodels\\Undead\\ImpaleTargetDust\\ImpaleTargetDust.mdl"
        constant string STUNORDERSTRING="thunderbolt"
        effect fx
        integer dbIndex=-1
        KnockDat array knockDB
        timer time=CreateTimer()
    endglobals

    function p()
        local integer index=0
        local real flyHeight
        local real unitX
        local real unitY
        local real newX
        local real newY
        local real vel2d
        local KnockDat tempDat
        loop
            exitwhen index>dbIndex
            tempDat=knockDB[index]
            unitX=GetUnitX(tempDat.u)
            unitY=GetUnitY(tempDat.u)
            newX=unitX + tempDat.xOffs
            newY=unitY + tempDat.yOffs
            flyHeight=GetUnitFlyHeight(tempDat.u)
            vel2d=(tempDat.xOffs*tempDat.xOffs + tempDat.yOffs*tempDat.yOffs)
            if flyHeight<MINFLYHEIGHT
                if vec2(newX, newY).isTerrainWalkable()
                    SetUnitX(tempDat.u,unitX + tempDat.xOffs)
                    SetUnitY(tempDat.u,unitY + tempDat.yOffs)
                    tempDat.xOffs=tempDat.xOffs*(1-FRICTION)
                    tempDat.yOffs=tempDat.yOffs*(1-FRICTION)
                    SetUnitMoveSpeed(tempDat.u,GetUnitDefaultMoveSpeed(tempDat.u))
                    if tempDat.xOffs*tempDat.yOffs>MINFRICTIONFOREFFECTSQUARED
                        fx=AddSpecialEffect(FRICTIONMODEL,unitX,unitY)
                        DestroyEffect(fx)
                    endif
                else
                    tempDat.xOffs=0
                    tempDat.yOffs=0
                    if vel2d>MINFORSTUN
                        UnitAddAbility(globalDummy, ID_STUN)
                        IssueTargetOrder(globalDummy,STUNORDERSTRING,tempDat.u)
                        UnitRemoveAbility(globalDummy, ID_STUN)
                    endif
                endif
                if tempDat.zOffs<MAXZVELOCITYTOBOUNCE
                    tempDat.zOffs=tempDat.zOffs*-1.*BOUNCECOEFFICIENT
                endif
                if tempDat.zOffs>MINZVELOCITYTOBECOMEAIRBORNE
                    SetUnitFlyHeight(tempDat.u,flyHeight + tempDat.zOffs,0)
                    tempDat.zOffs=tempDat.zOffs-GRAVITY
                endif
            else
                tempDat.zOffs=tempDat.zOffs-GRAVITY
                let heightDifference = vec2(newX, newY).getTerrainZ() - vec2(unitX, unitY).getTerrainZ()
                SetUnitFlyHeight(tempDat.u,flyHeight + tempDat.zOffs-heightDifference,0)
                SetUnitX(tempDat.u,newX)
                SetUnitY(tempDat.u,newY)
                SetUnitMoveSpeed(tempDat.u,0)
            endif
            if vel2d<MINFORKNOCKBACK and tempDat.zOffs>MAXZVELOCITYTOBOUNCE and tempDat.zOffs<-1*MAXZVELOCITYTOBOUNCE and flyHeight<MINFLYHEIGHT
                knockDB[index]=knockDB[dbIndex]
                dbIndex=dbIndex-1
                SetUnitFlyHeight(tempDat.u,0,0)
                SetUnitMoveSpeed(tempDat.u,GetUnitDefaultMoveSpeed(tempDat.u))
                destroy tempDat
                if dbIndex<0
                    PauseTimer(time)
                endif
            endif
            index=index + 1
        endloop
    end

    function getUnitIndexFromStack takes unit u returns integer
        local integer index=0
        local integer returner=-1
        local KnockDat tempDat
        loop
            exitwhen index>dbIndex or returner!=-1
            tempDat=knockDB[index]
            if tempDat.u==u
                returner=index
            endif
            index=index + 1
        endloop
        return returner
    end

    public function addKnockback takes unit u, real power, real direction, real trajectory returns nothing
        let index = getUnitIndexFromStack(u)
        local KnockDat tempDat
        if index == -1
            tempDat = new KnockDat
            tempDat.u=u
            tempDat.xOffs=power*Cos(direction)*Cos(trajectory)
            tempDat.yOffs=power*Sin(direction)*Cos(trajectory)
            tempDat.zOffs=power*Sin(trajectory)
            dbIndex=dbIndex + 1
            knockDB[dbIndex]=tempDat
            UnitAddAbility(tempDat.u,CROWID)
            UnitRemoveAbility(tempDat.u,CROWID)
            if dbIndex==0
                TimerStart(time,.03,true,function p)
            endif
        else
            tempDat=knockDB[index]
            tempDat.xOffs=tempDat.xOffs + power*Cos(direction)*Cos(trajectory)
            tempDat.yOffs=tempDat.yOffs + power*Sin(direction)*Cos(trajectory)
            tempDat.zOffs=tempDat.zOffs + power*Sin(trajectory)
        endif
    end
endlibrary
