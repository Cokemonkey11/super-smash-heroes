package Hitscan
    import ClosureForGroups

    constant real PRECISION = .5
    constant real DEFAULT_WIDTH = 70.
    constant grp = CreateGroup()

    public tuple maybe_unit(bool have_unit, unit which)

    public function maybe_unit_of(unit which) returns maybe_unit
        return maybe_unit(which != null, which)

    public interface Consumer<T>
        function apply(unit u)

    public function maybe_unit.if_present(Consumer<unit> consumer)
        if this.have_unit
            consumer.apply(this.which)
        destroy consumer

    public function populate_group_with_hitscan(group input, vec2 start, vec2 finish, real width)
        let diff = finish - start
        let offs = start.angleTo(finish).direction(width*PRECISION)
        let dist = diff.length()
        var iter = start
        let iterations = R2I(dist/offs.length())

        for index = 0 to iterations
            iter += offs

            forUnitsInRange(iter, width/2.) (unit u) ->
                input.add(u)

    public function hitscan_first(vec3 start, vec3 finish, group source_set) returns maybe_unit
        // Down-select units from the source-set based on hitscan-length.
        let len = start.distanceToSq(finish)
        grp.clear()
        for u in source_set
            if start.distanceToSq(u.getPos3Real()) <= len
                grp.add(u)

        // Iterate.
        let diff = finish - start
        let offs = start.normalizedPointerTo(finish) * DEFAULT_WIDTH * PRECISION
        let dist = diff.length()
        var iter = start
        let iterations = R2I(dist/offs.length())

        for index = 0 to iterations
            iter += offs
            for u in grp
                if u.getPos3Real().distanceToSq(iter) < DEFAULT_WIDTH * DEFAULT_WIDTH
                    return maybe_unit_of(u)

        return maybe_unit_of(null)
