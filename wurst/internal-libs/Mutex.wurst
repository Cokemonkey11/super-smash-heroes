package Mutex

import ClosureTimers
import ErrorHandling
import LinkedList
import Reference


public interface LockBody
    function run(MutexContext destroyHandle)


public class MutexContext
    Mutex owner
    LockBody body

    construct(Mutex mutex, LockBody body)
        this.owner = mutex
        this.body = body

    ondestroy
        destroy this.body
        assertTrue(this.owner.bodies.dequeue() == this)
        this.owner.startNextIfPresent()

    function start()
        this.body.run(this)


public class Mutex
    var locked = false
    var bodies = new LinkedList<MutexContext>()

    construct()
        skip

    ondestroy
        bodies.forEach() (MutexContext t) ->
            error("Lock body poisoned - " + (t castTo int).toString())
        destroy bodies

    function run(LockBody body)
        this.bodies.push(new MutexContext(this, body))
        if this.bodies.size() == 1
            startNextIfPresent()

    function runFallible(LockBody body) returns bool
        if not this.bodies.isEmpty()
            return false

        let context = new MutexContext(this, body)
        this.bodies.push(context)
        context.start()
        return true

    function startNextIfPresent()
        if not this.bodies.isEmpty()
            this.bodies.getFirst().start()


@Test function test_mutex_simple()
    let mtex = new Mutex()

    let ref = new Reference<int>(0)

    mtex.runFallible() (MutexContext destroyHandle) ->
        ref.val = 42
        destroy destroyHandle

    assertTrue(ref.val == 42, "val was 42")

    mtex.runFallible() (MutexContext destroyHandle) ->
        ref.val = 99
        destroy destroyHandle

    assertTrue(ref.val == 99, "val was 99")

    mtex.run() (MutexContext destroyHandle) ->
        ref.val = 1
        doAfter(1.) ->
            ref.val = 2
            destroy destroyHandle

    mtex.run() (MutexContext destroyHandle) ->
        doAfter(1.) ->
            ref.val = -1
            destroy destroyHandle

    assertTrue(ref.val == 1, "val was 1")
    doAfter(1.1) ->
        assertTrue(ref.val == 2, "val was 2")

        doAfter(1.) ->
            assertTrue(ref.val == -1, "val was -1")
            assertTrue(mtex.bodies.isEmpty(), "bodies was empty")
