[CENTER]
[TABLE][TR][TD]
[CENTER]
[H3][color=#60A600]Super Smash Heroes[/color][/H3]
[color=#CCAA00][B]A map by Cokemonkey11[/B][/color]

[BOX=Contents]* Introduction
* Screenshots
* Heroes
* Version Control
* Changelog
* Credits
* Contributing
[/BOX]
[/CENTER]

[/TD][/TR][TR][TD][H3][color=#CCAA00]Introduction[/color][/H3][/TD][/TR][TR][TD]
Super Smash Heroes is an arena fighting game where you use WC3-style abilities to knock
your foes off the map.

Dynamic movement and unique spells provide a fun g4mer experience.

Re-live your smash bros childhood in this polished and modern custom game.

[/TD][/TR][TR][TD][H3][color=#CCAA00]Screenshots[/color][/H3][/TD][/TR][TR][TD]


[hidden=Hero selection]
[img]https://i.imgur.com/n4WuY03.png[/img]
[/hidden]

[hidden=Jump ability]
[img]https://i.imgur.com/rXimoW0.png[/img]
[/hidden]

[hidden=Shadow seer's augmented attack]
[img]https://i.imgur.com/84U0Mmt.png[/img]
[/hidden]

[hidden=Shadow seer's attack tooltip]
[img]https://i.imgur.com/5kExkxK.png[/img]
[/hidden]

[hidden=Shadow seer's dark storm ability]
[img]https://i.imgur.com/AtJ6JQ2.png[/img]
[/hidden]

[/TD][/TR][TR][TD][H3][color=#CCAA00]Heroes[/color][/H3][/TD][/TR][TR][TD]


[TABLE]
[TR]
[TD][img]https://i.imgur.com/XlSMV4I.png[/img][/TD]
[TD][b]Orcish Swordmaster[/b]: Melee hero with high mobility.[/TD]
[/TR]
[TR]
[TD][img]https://i.imgur.com/Dhau7VX.png[/img][/TD]
[TD][b]Death's Mage[/b]: Caster that can channel spells whilst invisible.[/TD]
[/TR]
[TR]
[TD][img]https://i.imgur.com/5k1fhMQ.png[/img][/TD]
[TD][b]Underlord[/b]: Caster with high utility. Adept at area damage and deception.[/TD]
[/TR]
[TR]
[TD][img]https://i.imgur.com/BVoIUfN.png[/img][/TD]
[TD][b]Shadow Seer[/b]: Caster that augments its basic attack.[/TD]
[/TR]
[TR]
[TD][img]https://i.imgur.com/0tCQE9Z.png[/img][/TD]
[TD][b]High Naga Invigilator[/b]: Melee assassin that can counter-attack.[/TD]
[/TR]
[/TABLE]

[/TD][/TR][TR][TD][H3][color=#CCAA00]Version Control[/color][/H3][/TD][/TR][TR][TD]


All iterations of this map are maintained in a public git repository at
[url]https://bitbucket.org/Cokemonkey11/super-smash-heroes/[/url]

[/TD][/TR][TR][TD][H3][color=#CCAA00]Changelog[/color][/H3][/TD][/TR][TR][TD]


[color=#ffcc00]0.11.0[/color] [color=#999999]25 November 2022[/color]:
[list][*] Added a second fight zone and implemented map-selection.
[/list]

[color=#ffcc00]0.10.2[/color] [color=#999999]23 November 2022[/color]:
[list][*] Fixed a bug related to player leaving during hero select.
[*] Added a 30 second timer to hero select before players are given a random hero.
[*] Fixed a bug where Naga Torrent could get stuck due to acceleration being too slippery.
[*] Removed hotkey tooltips since they are not needed in reforged and wrong in reforged
grid mode.
[*] Changed the hotkey for Jump from Z to F to better suit German keyboards.
[/list]

[color=#ffcc00]0.10.1[/color] [color=#999999]13 May 2022[/color]:
[list][*] Fixed a bug where Underlord's block ability didn't last as long as it says in the
tooltip.
[*] Fixed a bug where Invulnerable units could still be thrown and damaged.
[*] Fixed a bug where Naga Blade Artist and Torrent would be unstable if cast whilst
airborne.
[*] Fixed a regression where Death's Mage missiles would sometimes not appear.
[*] Increased the attack cooldown for Shadow Seer from 1.1 to 1.3 seconds.
[*] Increased the cooldown for Swordmaster's Hall of Mirrors from 11 to 12 seconds.
[*] You can now repick your hero at the start of the round if you haven't been in combat.
[/list]

[color=#ffcc00]0.10.0[/color] [color=#999999]12 May 2022[/color]:
[list][*] Fixed a bug where High Naga Invigilator Torrent and Blade Artist could get stuck when
thrown.
[*] The game now restarts after fewer than two combatants are left.
[*] Increased High Naga Invigilator's Torrent range from 800 to 1200.
[*] Increased the dash speed of Torrent.
[*] Death's Mage's attack and barrage now fire all missiles in the target direction (was:
facing direction).
[*] Improved tooltips for Shadow Seer wards.
[*] Reduced Invigilator attack cast time from 0.4 to 0.3s.
[*] Fixed a bug where the game would not end if a player left before choosing a hero.
[*] Combatants no longer respawn if they've left the game.
[*] Naga High Invigilator's Blade Artist ability now throws in the target direction (was:
angle of incidence).
[*] Fixed a minor graphical bug that would show up at the top left of the map.
[/list]

[color=#ffcc00]0.9.8[/color] [color=#999999]14 Apr 2021[/color]:
[list][*] New hero: High Naga Invigilator.
[*] Attack abilities are now targeted spells to ease aiming.
[*] Fixed: Underlord block was not working.
[*] Fixed: Orcish Swordmaster War Slam was not instant cast.
[/list]

[hidden=Older Changes][color=#ffcc00]0.9.7[/color] [color=#999999]12 Apr 2021[/color]:
[list][*] Fixed: getting stuck on cliffs.
[*] Balance: Orcish Swordmaster War Slam reduced range, reduced Throw, increased cooldown.
[*] Balance: Orcish Swordmaster Hall of Mirrors reduced Throw.
[*] Balance: Reduced Orcish Swordmaster's mass (for Throw) by 10%.
[*] Removed invalid tga map preview file.
[*] Added missing resource credit for kitabatake.
[/list]

[color=#ffcc00]0.9.6[/color] [color=#999999]28 Mar 2021[/color]:
[list][*] Fixed: heroes obtained their wc3 attack if owning player leaves.
[*] Fixed: being knocked into the bridge should no longer destroy it.
[*] Updated map preview image.
[/list]

[color=#ffcc00]0.9.5[/color] [color=#999999]26 Mar 2021[/color]:
[list][*] Initial release.
[/list]

[/hidden][/TD][/TR][TR][TD][H3][color=#CCAA00]Credits[/color][/H3][/TD][/TR][TR][TD]
peq, Frotty, lep, Wrda, alfredx_sotn, Kitabatake, Nintendo

[/TD][/TR][TR][TD][H3][color=#CCAA00]Contributing[/color][/H3][/TD][/TR][TR][TD]
I will merge atomic, well-formed pull-requests if they are consistent with my design
policies and issue tracker.

[/TD][/TR][/TABLE]
[/CENTER]
